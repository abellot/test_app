import { LANGUAGES } from '@module-shared/enums/languages';

export const environment = {
  production: true,
  // Languages
  LANGUAGE: {
    DEFAULT: LANGUAGES.ES,
  },
};
