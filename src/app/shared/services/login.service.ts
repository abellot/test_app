import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginRequest } from '@module-shared/models/login-request';
import { CrudService } from './crud.service';

@Injectable({
  providedIn: 'root',
})
export class LoginService extends CrudService {
  constructor(httpClient: HttpClient) {
    super(httpClient, null, null);
  }

  login(data: LoginRequest): boolean {
    return (data.username === 'root@soprasteria.com' && data.password === 'matrix');
  }
}
