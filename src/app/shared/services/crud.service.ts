import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { paramsToString } from '@module-shared/utils/service';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class CrudService {
	urlBase: string;

	constructor(
		private httpClient: HttpClient,
		@Inject(String) private module: string,
		@Inject(String) private url?: string,
	) {
		if (!this.module) {
			this.module = '';
		}

		if (this.url) {
			this.urlBase = this.url;
		}
	}

	create(
		endpoint: string,
		item: any,
		jsonType: boolean = true,
	): Observable<any> {
		const contentType = jsonType
			? 'application/json'
			: 'application/x-www-form-urlencoded';
		const headers = new HttpHeaders({
			'Content-Type': contentType,
		});
		return this.httpClient.post(
			`${this.urlBase}${this.module}${endpoint}`,
			item,
			{ headers },
		);
	}


	read(endpoint: string): Observable<any> {
		return this.httpClient.get(`${this.urlBase}${this.module}${endpoint}`).pipe(
			map((response: any) => {
				return response;
			}),
			catchError((error) => {
				return throwError(error);
			}),
		);
	}


	update(endpoint: string, params: any, item: any): Observable<any> {
		const extraParams = paramsToString(params);
		return this.httpClient
			.put<any>(`${this.urlBase}${this.module}${endpoint}${extraParams}`, item)
			.pipe(
				map((response: any) => {
					return response;
				}),
				catchError((error) => {
					return throwError(error);
				}),
			);
	}

	delete(endpoint: string, item: any) {
		return this.httpClient
			.delete(`${this.urlBase}${this.module}${endpoint}`)
			.pipe(
				map((response: any) => {
					return response;
				}),
				catchError((error) => {
					return throwError(error);
				}),
			);
	}
}
