export const paramsToString = (request: any): string => {
  return Object.keys(request)
    .map((currentKey: string) => ({
      key: currentKey,
      value: request[currentKey],
    }))
    .reduce((paramString: string, param: { key: string; value: any }) => {
      const symbol = paramString ? '&' : '?';
      return `${paramString}${symbol}${param.key}=${param.value}`;
    }, '');
};
