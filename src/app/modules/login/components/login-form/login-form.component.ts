import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { AnimationController } from '@ionic/angular';
import { LoginRequest } from '@module-shared/models/login-request';
import { LoginService } from '@module-shared/services/login.service';
import { ToastService } from '@module-shared/services/toast.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  @ViewChild('loginBox', { read: ElementRef }) loginBox: ElementRef;

  try: number = 4;

  loginForm = this.formBuilder.group({
    username: new FormControl(null, [Validators.required, Validators.email]),
    password: new FormControl(null, [
      Validators.required,
      Validators.minLength(5),
    ]),
    remember: new FormControl(false),
  });

  constructor(
    private formBuilder: FormBuilder,
    private toastSvc: ToastService,
    private animationCtrl: AnimationController,
    private loginSvc: LoginService,
  ) {}

  ngOnInit() {}

  sendLogin() {
    if (this.loginForm.valid) {
      const data: LoginRequest = {
        username: this.loginForm.controls.username.value,
        password: this.loginForm.controls.password.value,
        remember: this.loginForm.controls.remember.value,
      };
      if(this.loginSvc.login(data)){
        this.toastSvc.presentToast('Conectado');
      } else {
        this.discountTry();
      }
    } else {
      this.discountTry();
    }
  }

  discountTry() {
    this.try--;
    if (this.try !== 0) {
      this.toastSvc.presentToast(
        `Usuario incorrecto, quedan ${this.try} intentos`
      );
    } else {
      this.toastSvc.presentToast(
        'Deberias probar con email: root@soprasteria.com password: matrix'
      );
    }
    const shakeAnimation = this.animationCtrl
      .create('shake')
      .addElement(this.loginBox.nativeElement)
      .duration(150)
      .iterations(5)
      .fromTo('transform', 'translateX(0px)', 'translateX(50px)')
      .fromTo('transform', 'translateX(0px)', 'translateX(-50px)')
      .fromTo('transform', 'translateX(-50px)', 'translateX(0px)');
    shakeAnimation.play();
  }
}
