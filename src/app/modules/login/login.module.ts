import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '@module-shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@module-material/material.module';
import { LoginRoutingModule } from './login-routing.module';
import { LoginPage } from './pages/login/login.page';
import { LoginFormComponent } from './components/login-form/login-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    LoginRoutingModule,
    SharedModule,
    TranslateModule.forChild(),
    MaterialModule,
  ],
  declarations: [LoginPage, LoginFormComponent],
  exports: [LoginFormComponent],
  providers: [],
})
export class LoginModule {}
