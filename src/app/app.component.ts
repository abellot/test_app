import { Component } from '@angular/core';
import { TranslateConfigService } from '@module-shared/services/translate-config.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(private translateConfigService: TranslateConfigService) {
    this.translateConfigService.setLanguage(environment.LANGUAGE.DEFAULT);
  }
}
